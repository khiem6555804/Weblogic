package com.example.firstjpa.repository;


import com.example.firstjpa.model.Chapter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ChapterRepository extends JpaRepository<Chapter,Long> {
    @Query(value = "SELECT * FROM Chapter",
            nativeQuery = true)
    List<Chapter> findAllActiveUsers(Pageable pageable);
    @Query(value = "SELECT * FROM Chapter",
            nativeQuery = true)
    List<Chapter> findChapterByChapName(String ChapName);
}
