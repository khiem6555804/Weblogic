package com.example.firstjpa.controller;

import com.example.firstjpa.model.Chapter;
import com.example.firstjpa.repository.ChapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ChapterController {
    @Autowired
    private ChapterRepository chapterRepository;
    @GetMapping("employees/{id}")
    public List<Chapter> getAllEmployees(@PathVariable(value = "id") int id) {

        return chapterRepository.findAll(PageRequest.of(id, 5)).stream().toList();
    }
    @GetMapping("name/{name}")
    public List<Chapter> getAllEmployeesName(@PathVariable(value = "name") String name) {

        return chapterRepository.findChapterByChapName(name);
    }
    @GetMapping
    public List<Chapter> getAll() {

        return chapterRepository.findAll().stream().toList();
    }
    public ChapterController(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }
}
