package com.example.firstjpa.controller;

import com.example.firstjpa.model.Chapter;
import com.example.firstjpa.repository.ChapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/emp")
public class EmployeeController {
    @Autowired
    private ChapterRepository chapterRepository;

    @GetMapping
    public List<Chapter> getAll() {

        return chapterRepository.findAll().stream().toList();
    }
    @GetMapping("ok")
    public String it() {

        return "sadsa";
    }
}
