package com.example.firstjpa.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class GirlFriend {
    @Autowired
    Outfit outfit;

}
