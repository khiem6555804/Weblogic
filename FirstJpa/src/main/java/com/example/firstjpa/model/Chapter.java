package com.example.firstjpa.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Chapter")
public class Chapter {
    @Id
    @GeneratedValue
    @Column(name = "chapid")
    private int chapId;
    @Column(name = "courseId")

    private int courseId;
    @Column(name = "chapname")
    private String chapName;
    @Column(name = "description")
    private String description;

    public Chapter(int chapId, int courseId, String chapName, String description) {
        this.chapId = chapId;
        this.courseId = courseId;
        this.chapName = chapName;
        this.description = description;
    }

    public Chapter() {

    }

    public int getChapId() {
        return chapId;
    }

    public void setChapId(int chapId) {
        this.chapId = chapId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getChapName() {
        return chapName;
    }

    public void setChapName(String chapName) {
        this.chapName = chapName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
