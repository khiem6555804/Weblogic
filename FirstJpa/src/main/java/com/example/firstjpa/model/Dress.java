package com.example.firstjpa.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class Dress implements Outfit{
    private String name;
    private int id;


    @Override
    public String getColour() {
        return "blue";
    }
}
