package com.example.firstjpa.model;

public interface Outfit {
     String getColour();
}
